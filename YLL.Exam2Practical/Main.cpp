// Exam 2 Practical
// Yee Lor

#include <iostream>
#include <conio.h>
#include <fstream>
#include <string>

using namespace std;
const int numCount = 5;
float numbers[numCount];

void collectNum(float number[])
{
    for (int i = 0; i < numCount; i++) {
        cout << "Enter Number " << i + 1 << ": ";
        cin >> number[i];
    }
}

void print(float number[], float avg, float max, float min)
{
    cout << "The numbers you entered are: ";
    for (int i = 0; i < numCount; i++)
    {
        cout << number[i] << " ";
    }
    cout << "\n";
    cout << "The average is: " << avg;
    cout << "\n";
    cout << "The maximum number is: " << max;
    cout << "\n";
    cout << "The minimum number is: " << min;
    // Extra Credit
    cout << "\n";
    cout << "The numbers in reverse are: ";
    for (int i = numCount; i > 0; i--)
    {
        cout << number[i - 1] << " ";
    }
}

float FindAverage(float number[])
{
    float num = 0;
    for (int i = 0; i < numCount; i++) 
    {
        num += number[i];
    }
    num /= numCount;
    return num;
}

float FindMax(float number[])
{
    float num = number[0];
    for (int i = 0; i < numCount; i++)
    {
        if (number[i] > num)
        {
            num = number[i];
        }
    }
    return num;
}

float FindMin(float number[])
{
    float num = number[0];
    for (int i = 0; i < numCount; i++)
    {
        if (number[i] < num)
        {
            num = number[i];
        }
    }
    return num;
}

int main()
{
    cout << "Please enter five numbers." << "\n";
    collectNum(numbers);
    cout << "\n";
    print(numbers, FindAverage(numbers), FindMax(numbers), FindMin(numbers));
    cout << "\n" << "\n";

    cout << "Would you like to save your results? [Y for yes, N for no] ";
    char save;
    cin >> save;
    if (save == 'y' || save == 'Y')
    {
        string path = "Results.txt";
        ofstream savefile(path);

        savefile << "The numbers you entered are: ";
        for (int i = 0; i < numCount; i++)
        {
            savefile << numbers[i] << " ";
        }        
        savefile << "\n";
        savefile << "The average of is: " << FindAverage(numbers);
        savefile << "\n";
        savefile << "The maximum number of is: " << FindMax(numbers);
        savefile << "\n";
        savefile << "The minimum number of is: " << FindMin(numbers);
        savefile << "\n";
        savefile << "The numbers in reverse are: ";
        for (int i = numCount; i > 0; i--)
        {
            savefile << numbers[i - 1] << " ";
        }
        savefile.close();

        cout << "\n";
        cout << "The results were saved!";
        cout << "\n" << "\n";
        cout << "Press any key to exit...";
    }
    else
    {
        exit(EXIT_FAILURE);
    }

    _getch();
    return 0;
}